#ifndef _SER_H_
#define _SER_H_

#include "mbed.h"
#include "SoftPWM.h"

class SER
{
public:
    SER(PinName pwm);
    void  deg(int degree);
private:
    SoftPWM Pwm;
};

#endif
