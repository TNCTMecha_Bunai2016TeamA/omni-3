#ifndef _PIN_H_
#define _PIN_H_

#define pin_pwm_F p21
#define pin_dere_F p20

#define pin_pwm_L p22
#define pin_dere_L p19

#define pin_pwm_R p23
#define pin_dere_R p18

#define serial_tx p28
#define serial_rx p27

#define pin_led1 p5
#define pin_led2 p6
#define pin_led3 p7
#define pin_led4 p8

#define pin_sw1 p10
#define pin_sw2 p11
#define pin_hand p12

#define pin_dir_el p17
#define pin_pwm_el p24

#define pin_belt p25
#define pin_servo p26
#define pin_ping p29

#endif
