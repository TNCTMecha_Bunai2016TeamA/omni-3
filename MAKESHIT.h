#ifndef MBED_MAKESHIT
#define MBED_MAKESHIT

#include "mbed.h"
#include "MD.h"
#include "SER.h"

#define temp 18.0
#define hold 2.0
#define ST 4.0
#define DT 5.0
#define open 80
#define close 0

//MAKESHIT gentle(p29,p24,p17,p26,p25,p10,/*p10,*/p11,p12);基盤に合わせました
class MAKESHIT{
    public:
    MAKESHIT(PinName ping,PinName elpwm,PinName eldir,PinName hand,PinName belt,PinName sw1,PinName sw3,PinName cha);
    void boom(void);

    private:
    DigitalInOut _ping;
    MD _el;
    SER _hand;
    DigitalOut _belt;
    InterruptIn _sw1;
    InterruptIn _sw3;
    InterruptIn _charge;
    DigitalIn _CHA;
    //DigitalOut _dev1;
    //DigitalOut _dev2;

    void top(void);
    void middle(void);
    void bottom(void);
    void belser(void);
    void grasp(void);


    Timer _dist;
    Timer _cabal;
    Timeout _beltstop;
    Timeout _leave;
    double _ance;

    unsigned short hirai;

    /*
    What's hirai?
    ~Hirai resister is hirai's resister.
          Use the google honyaku,Please.~

    |bit15|14   |13   |12   |11   |10   |9    |bit8 |
    |sone |stam |midm |botm |open |cath |exam |smfl |

    |bit7 |6    |5    |4    |3    |2    |1    |bit0 |
    |eldi |flee |flee |flee |flee |flee |flee |flee |
    */

};

#endif
